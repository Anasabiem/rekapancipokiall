<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {
	function __construct()
	{
		parent::__construct();
        $this->load->model('M_transaksi');
				$this->load->model('Core');
        $this->load->model('M_keluar');
	}
	public function index()
	{
		if (!$this->session->userdata('action')=='login') {
			$this->session->set_flashdata("Pesan",$this->Core->alert_time('Not Access, Anda Harus Login'));
			redirect(base_url('Login'));
		} else {
			if ($this->session->userdata('status') =='admin') {
				$data['menu'] = $this->M_user->select('menu')->result();
				$idKaryawan = $this->session->userdata('id_karyawan');
				$data['total_uangadm'] = $this->M_transaksi->hitungJumlahUangadm();
				$data['total_assetadm'] = $this->M_transaksi->totalPesananwhereadm();
				$data['total_uangadm1'] = $this->M_keluar->dataUangall($idKaryawan);
				$data['total_assetadmkl'] = $this->M_keluar->totalbeliall();
				$data['jawam'] = $this->M_transaksi->filter('jawa');
				$data['jawak'] = $this->M_keluar->filterk('jawa');
				$data['polresm'] = $this->M_transaksi->filter('polres');
				$data['polresk'] = $this->M_keluar->filterk('polres');
				$data['patrangm'] = $this->M_transaksi->filter('patrang');
				$data['patrangk'] = $this->M_keluar->filterk('patrang');
				$this->load->view('v_dashboard',$data);
			} else {
				$this->session->set_flashdata("Pesan",$this->Core->alert_time('Not Access, Anda Harus Login'));
				$this->load->view('side/error');
			}
		}
	}
	function menu(){
		$this->load->view('adm/v_menu');
	}
}
