<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {
function __construct()
    {
        parent::__construct();
        // $this->load->model('M_transaksi');
         $this->load->model('Core');
        // $this->load->model('M_barang');
    }
	public function index()
	{
		$this->load->view('login');
	}
	function go(){
		$username = $this->input->post('username');
		$password = $this->input->post('password');
		$where = array(
			'username' => $username,

		);

		$cek = $this->M_user->cek_login("karyawan",$where)->num_rows();
		$cek1 = $this->M_user->cek_login("karyawan",$where)->row();
		if ($cek > 0) {
			$hash = $cek1->password;
			if (password_verify($password, $hash)) {
				$data = array(
					'id_karyawan' => $cek1->id_karyawan,
					'nama'=>$cek1->nama_karyawan,
					'username' => $cek1->username,
					'outlet' => $cek1->outlet,
					'status' =>'karyawan',
					'hint'=>$cek1->hint,
					'action'=>'login'

				);
				$this->session->set_userdata($data);
				$user = $this->session->userdata('id_karyawan');
				// die(var_dump($user));
				$data1 = array('status' =>'login');
				// $where['id_karyawan'] = $id;
				$upload = $this->db->update('karyawan',$data1,array('id_karyawan'=>$this->session->userdata('id_karyawan')));
				if ($upload > 0) {
					echo "Sukses";
					$this->session->set_userdata($data);
				$this->session->set_flashdata("Pesan",$this->Core->alert_succes("Login Berhasil"));
					redirect(base_url('Home'));	
				}else{
					$this->session->set_flashdata("Pesan",$this->Core->alert_time("Gagal, cek koneksi"));
					redirect(base_url('Login'));	
				}
			}else{
				$this->session->set_flashdata("Pesan",$this->Core->alert_time("Gagal, Password Salah"));
					redirect(base_url('Login'));
			}
		}else{
			$cek = $this->M_user->cek_login("admin",$where)->num_rows();
			$cek1 = $this->M_user->cek_login("admin",$where)->row();
			if ($cek > 0) {
				$hash = $cek1->password;
				if (password_verify($password, $hash)) {
					$data = array(
						'id_admin' => $cek1->id_admin,
						'namaa'=>$cek1->nm_admin,
						'username' => $cek1->username,
						'status' =>'admin',
						'action'=>'login'

					);
					// echo "Sukses";
					$this->session->set_userdata($data);
					$this->session->set_flashdata("Pesan",$this->Core->alert_succes("Login Berhasil"));
					redirect(base_url('Dashboard'));
					
				}else{
					$this->session->set_flashdata("Pesan",$this->Core->alert_time("Password Salah"));
					redirect(base_url('Login'));	
				}
			}
			else{
				$this->session->set_flashdata("Pesan",$this->Core->alert_time("Tidak Ada Username dan password terdaftar"));
					redirect(base_url('Login'));	
			}
		}
	}
	function logout(){
		if ($this->session->userdata('status')=='karyawan') {
			$data1 = array('status' =>'logout' );
			$upload = $this->db->update('karyawan',$data1,$this->session->userdata('id'));
			if ($upload > 0) {
				$this->session->sess_destroy();
				// $this->session->set_flashdata("Pesan",$this->core->alert_succes("Login sukses"));
				redirect(base_url('Login'));
			}
		} else {
			$this->session->sess_destroy();
			// $this->session->set_flashdata("Pesan",$this->core->alert_succes("Login sukses"));
			redirect(base_url('Login'));
		}
	}
}
