<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->model('M_transaksi');
		$this->load->model('Core');
        // $this->load->model('M_barang');
	}
	public function index()
	{
		if (!$this->session->userdata('action')=='login') {
			$this->session->set_flashdata("Pesan",$this->Core->alert_time('Not Access, Anda Harus Login'));
			redirect(base_url('Login'));
		} else {
			if ($this->session->userdata('status') =='karyawan') {
				$data['menu'] = $this->M_user->select('menu')->result();
				$this->load->view('v_home',$data);
			} else {
				$this->session->set_flashdata("Pesan",$this->Core->alert_time('Not Access, Anda Harus Login'));
				$this->load->view('side/error');
			}
			
		}
	}
	function masuk(){
		$menu = $this->input->post('menu');
		$saos = $this->input->post('saos');
		$sambal = $this->input->post('sambal');
		$nama = $this->input->post('nmPs');
		$harga = $this->input->post('harga');
		$totalbeli = $this->input->post('totalhar');
		$bayar = $this->input->post('bayar');
		$date = date('Y-m-d');
		$time = gmdate("H:i:s", time()+60*60*7);
		$user = $this->session->userdata('id_karyawan');
		$data = array('nmPembeli' =>$nama ,
			'tanggal'=> $date,
			'jam'=>$time,
			'user'=>$user,
			'jenis'=>'biasa' );
		$simpanPmbeli =$this->db->insert('transaksi',$data);
		if ($simpanPmbeli > 0) {	
			$sikat = $this->db->insert_id();
			for ($i=0; $i < count($menu) ; $i++) {
				$data= array('nama_menu' =>$menu[$i],
					'saos'=>$saos[$i],
					'level'=>$sambal[$i],
					'idTransaksi'=>$sikat,
					'harga'=>$harga[$i] );
				$kirim = $this->db->insert('penjualan',$data);
			}
			$data  = array('idTransaksi' =>$sikat ,
				'total'=>$totalbeli );
			$selesai = $this->db->insert('dttransaksi',$data);
			if ($selesai > 0) {
				$hasil = $bayar - $totalbeli;
				if ($hasil < 0) {
				$this->session->set_flashdata("Pesan",$this->Core->alert_time('Gagal Pembayaran harus Lebih Besar Dari Total Pembelian'));	
				redirect(base_url('Home'));
				} else {
					if ($totalbeli <= 0) {
						$this->session->set_flashdata("Pesan",$this->Core->alert_time('Gagal Menu Belum anda pilih'));
					} else {
						$this->session->set_flashdata("Pesan",$this->Core->alert_kmb($hasil));
				redirect(base_url('Home'));
					}
				}
			} else {
				$this->session->set_flashdata("Pesan",$this->Core->alert_time('Gagal Cek Pembayaran'));
				redirect(base_url('Home'));
			}
		} else {
			$this->session->set_flashdata("Pesan",$this->Core->alert_time('Gagal Simpan detail'));
		}

	}
	function hint(){
		$hint = $this->input->post('hint');
		$where['id_karyawan']=$this->session->userdata('id_karyawan');
		$data = array('hint' =>$hint);
		$ins = $this->db->update('karyawan',$data,$where);
		if ($ins > 0) {
			$cek1 = $this->M_user->cek_login("karyawan",$where)->row();
			$data1 = array('hint' =>$cek1->hint);
			$this->session->set_userdata($data);
			$this->session->set_flashdata("Pesan",$this->Core->alert_succes('Data Berhasil Di perbarui'));
			redirect(base_url('Home'));
		} else {
			$this->session->set_flashdata("Pesan",$this->Core->alert_time('Data Gagal Di perbarui, Coba Lagi'));
			redirect(base_url('Home'));
		}
		
	}
}
