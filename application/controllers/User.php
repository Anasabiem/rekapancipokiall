<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {
	function __construct()
	{
		parent::__construct();
        // $this->load->model('M_transaksi');
		$this->load->model('Core');
        // $this->load->model('M_barang');
	}
	public function index()
	{
		if (!$this->session->userdata('action')=='login') {
			$this->session->set_flashdata("Pesan",$this->Core->alert_time('Not Access, Anda Harus Login'));
			redirect(base_url('Login'));
		} else {
			if ($this->session->userdata('status') =='admin') {
				$data['karyawan'] = $this->M_user->selectall();
				$this->load->view('v_karyawan',$data);
			} else {
				$this->session->set_flashdata("Pesan",$this->Core->alert_time('Not Access, Anda Harus Login'));
				$this->load->view('side/error');
			}

		}

	}
	public function insert(){
		$this->load->view('v_user');
	}
	function buat(){
		if(isset($_POST['btnSimpan'])){
			$config = array('upload_path' => './media/user/' ,'allowed_types' => 'gif|jpg|png|jpeg' );
			$this-> load -> library('upload', $config);
			if ($this->upload->do_upload('foto'))
			{
				$upload_data = $this -> upload -> data ();
				$nama = $this -> input -> post ('nama');
				$alamat = $this -> input -> post ('alamat');
				$bagian = $this -> input -> post ('bagian');
				$username = $this -> input -> post ('username');
				$password = $this -> input -> post ('password');
				$partner = $this -> input -> post ('partner');
				$out = $this -> input -> post ('out');
				$foto = "media/user/".$upload_data['file_name'];
				$hash = password_hash($password, PASSWORD_DEFAULT);
				$data = array(
					'nama_karyawan'=>$nama,
					'bagian'=>$bagian,
					'alamat'=>$alamat,
					'username'=>$username,
					'password'=>$hash,
					'status'=>'logout',
					'partner'=>$partner,
					'outlet'=>$out,
					'foto'=>$foto
				);
				$insert_data = $this->db->insert('karyawan',$data);
			}
			if ($insert_data >= 0) {
				$this->session->set_flashdata("Pesan",$this->Core->alert_succes("Data Tersimpan"));
				header('location:'.base_url("User"));
			} else{
				$this->session->set_flashdata("Pesan",$this->Core->alert_time("Gagal ! Coba Lagi"));
				header('location:'.base_url("User"));
			}
		}else{
			$this->session->set_flashdata("Pesan",$this->Core->alert_time("Gagal ! Coba Lagi"));
			redirect(base_url('User'));
		}
	}
// hapus karyawan
function hps($id){
	$where = array('id_karyawan' => $id);
	$hapus = $this->M_user->delete($where,'karyawan');
	if ($hapus >=0) {
		$this->session->set_flashdata("Pesan",$this->Core->alert_succes("Berhasil di Hapus"));
		header('location:'.base_url('User'));
	}else{
		header('location:'.base_url('User'));
		$this->session->set_flashdata("Pesan",$this->Core->alert_time("Gagal Hapus"));
	}
}


	function profile(){
		$this->load->view('v_profile');
	}

	function updateft(){
		if(isset($_POST['btnSimpan'])){
			$config = array('upload_path' => './media/user/' ,'allowed_types' => 'gif|jpg|png|jpeg' );
			$this-> load -> library('upload', $config);
			if ($this->upload->do_upload('foto'))
			{
				$upload_data = $this -> upload -> data ();
				$foto = "media/user/".$upload_data['file_name'];
				$data = array(
					'foto'=>$foto
				);
				$insert_data = $this->db->update('karyawan',$data);
			}
			if ($insert_data >= 0) {
				$this->session->set_flashdata("Pesan",$this->Core->alert_succes("Data Tersimpan"));
				header('location:'.base_url("User"));
			} else{
				$this->session->set_flashdata("Pesan",$this->Core->alert_time("Gagal ! Coba Lagi"));
				header('location:'.base_url("User"));
			}
		}else{
			$this->session->set_flashdata("Pesan",$this->Core->alert_time("Gagal ! Coba Lagi"));
			redirect(base_url('User'));
		}
	}
}
