<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Keluar extends CI_Controller {
	function __construct()
    {
        parent::__construct();
        $this->load->model('M_keluar');
         $this->load->model('Core');
        // $this->load->model('M_barang');
    }
	public function index()
	{
		if (!$this->session->userdata('action')=='login') {
			$this->session->set_flashdata("Pesan",$this->Core->alert_time('Not Access, Anda Harus Login'));
			redirect(base_url('Login'));
		} else {
			$idKaryawan = $this->session->userdata('id_karyawan');
			$dat = date('Y-m-d');
			$data['total_uang'] = $this->M_keluar->dataUang($dat,$idKaryawan);
			$data['total_uangadm'] = $this->M_keluar->dataUangall($idKaryawan);
			$data['total_asset'] = $this->M_keluar->totalbeliwhere($idKaryawan);
			$data['total_assetadm'] = $this->M_keluar->totalbeliall();
			$data['isi'] = $this->M_keluar->selectByUser($idKaryawan,$dat);
			$data['isiad']= $this->M_keluar->selectall($dat);
			$this->load->view('v_keluar',$data);
		}
	}
	function insert(){
		$tgl = date('Y-m-d');
		$time = gmdate("H:i:s", time()+60*60*7);
		$nama= $this->input->post('nama');
		$ktt= $this->input->post('ktt');
		$hrga =$this->input->post('hrga');
		$ttl= $ktt*$hrga;
		$kar =$this->session->userdata('id_karyawan');
		$data = array('nmBarang' =>$nama ,
					'tgl'=>$tgl,
					'jam'=>$time,
					'qty'=>$ktt,
					'ttlUang'=>$ttl,
					'idKaryawan'=>$kar );
		$masuk = $this->db->insert('pengeluaran',$data);
		if ($masuk > 0) {
			$this->session->set_flashdata("Pesan",$this->Core->alert_succes("Data Tersimpan"));
					redirect(base_url('Keluar'));	
		}else{
			$this->session->set_flashdata("Pesan",$this->Core->alert_time("Gagal ! Coba Lagi"));
					redirect(base_url('Keluar'));
		}

	}


}
