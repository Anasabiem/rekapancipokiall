<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Laporan extends CI_Controller {
	function __construct()
    {
        parent::__construct();
        $this->load->model('M_transaksi');
         $this->load->model('Core');
        // $this->load->model('M_barang');
    }
	public function index()
	{
		if (!$this->session->userdata('action')=='login') {
			$this->session->set_flashdata("Pesan",$this->Core->alert_time('Not Access, Anda Harus Login'));
			redirect(base_url('Login'));
		} else {
			$idKaryawan = $this->session->userdata('id_karyawan');
			$dat = date('Y-m-d');
		// die(var_dump($dat));
			$data['total_uang'] = $this->M_transaksi->hitungJumlahUang($idKaryawan);
			$data['total_uangadm'] = $this->M_transaksi->hitungJumlahUangadm();
			$data['total_asset'] = $this->M_transaksi->totalPesananwhere($idKaryawan);
			$data['total_assetadm'] = $this->M_transaksi->totalPesananwhereadm();
			$data['isi'] = $this->M_transaksi->get_by_user($idKaryawan,$dat);
			$data['isiad']= $this->M_transaksi->getAll($dat);
			$this->load->view('v_laporan',$data);
		}
	}


}
