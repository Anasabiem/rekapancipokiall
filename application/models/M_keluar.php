<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_keluar extends CI_Model {

  function selectByUser($kd,$dt){
    $this->db->where('karyawan.id_karyawan', $kd);
    $this->db->where('pengeluaran.tgl', $dt);
    $this->db->join("karyawan", "karyawan.id_karyawan=pengeluaran.idKaryawan","left");
    $this->db->order_by('pengeluaran.jam', 'DESC');
    return $this->db->get('pengeluaran');
  }
  function dataUang($dat,$idK){
    $dat = date('Y-m-d');
    $this->db->select("sum(pengeluaran.ttlUang) as 'total'");
    $this->db->from('pengeluaran');
    $this->db->where('pengeluaran.tgl', $dat);
    $this->db->where('pengeluaran.idKaryawan', $idK);
    $query = $this->db->get();
    return $query->row_array();
  }
  function selectall($dt){
    // $this->db->where('karyawan.id_karyawan', $kd);
    $this->db->where('pengeluaran.tgl', $dt);
    $this->db->join("karyawan", "karyawan.id_karyawan=pengeluaran.idKaryawan","left");
    $this->db->order_by('karyawan.outlet', 'DESC');
    return $this->db->get('pengeluaran');
  }
  function dataUangall($dat){
    $dat = date('Y-m-d');
    $this->db->select("sum(pengeluaran.ttlUang) as 'total'");
    $this->db->from('pengeluaran');
    $this->db->where('pengeluaran.tgl', $dat);
    // $this->db->where('pengeluaran.idKaryawan', $idK);
    $query = $this->db->get();
    return $query->row_array();
  }
  function totalbeliwhere($idK){
    $dat = date('Y-m-d');
      $this->db->from('pengeluaran');
      $this->db->where('pengeluaran.idKaryawan', $idK);
      $this->db->where('pengeluaran.tgl', $dat);
      return $this->db->count_all_results();
  }
  function totalbeliall(){
    $dat = date('Y-m-d');
      $this->db->from('pengeluaran');
      // $this->db->where('pengeluaran.idKaryawan', $idK);
      $this->db->where('pengeluaran.tgl', $dat);
      return $this->db->count_all_results();
  }
  function filterk($idK){
    $dat = date('Y-m-d');
    $this->db->select("sum(pengeluaran.ttlUang) as 'total'");
    $this->db->join('karyawan','karyawan.id_karyawan=pengeluaran.idKaryawan');
    $this->db->from('pengeluaran');
    $this->db->where('pengeluaran.tgl', $dat);
    $this->db->where('karyawan.outlet', $idK);
    $query = $this->db->get();
    return $query->row_array();
  }




}
