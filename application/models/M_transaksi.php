  <?php

  class M_transaksi extends CI_Model
  {

      public $table = 'transaksi';
      public $kd = 'idTransaksi';
      public $order = 'DESC';
      public $dt = 'dttransaksi';

      function __construct()
      {
          parent::__construct();
      }


    function totalPesananwhere($idK){
            // get total rows
      $dat = date('Y-m-d');
        $this->db->from($this->table);
        $this->db->where('transaksi.user', $idK);
        $this->db->where('transaksi.tanggal', $dat);
        // $this->db->where('transaksi.waktu',CURDATE());
        return $this->db->count_all_results();
    }
    function totalPesananwhereadm(){
            // get total rows
      $dat = date('Y-m-d');
        $this->db->from($this->table);
        // $this->db->where('transaksi.user', $idK);
        $this->db->where('transaksi.tanggal', $dat);
        // $this->db->where('transaksi.waktu',CURDATE());
        return $this->db->count_all_results();
    }
    public function hitungJumlahUang($idK)
    {
      $dat = date('Y-m-d');
      $this->db->select("transaksi.tanggal, sum(dttransaksi.total) as 'total'");
      $this->db->from('dttransaksi');
      $this->db->join('transaksi', 'dttransaksi.idTransaksi=transaksi.idTransaksi', 'inner');
      $this->db->where('transaksi.tanggal', $dat);
      $this->db->where('transaksi.user', $idK);
  $query = $this->db->get();
  // die(var_dump($query));
  return $query->row_array();
    }
    public function hitungJumlahUangadm()
    {
      $dat = date('Y-m-d');
      $this->db->select("transaksi.tanggal, sum(dttransaksi.total) as 'total'");
      $this->db->from('dttransaksi');
      $this->db->join('transaksi', 'dttransaksi.idTransaksi=transaksi.idTransaksi', 'inner');
      $this->db->where('transaksi.tanggal', $dat);
      // $this->db->where('transaksi.user', $idK);
  $query = $this->db->get();
  // die(var_dump($query));
  return $query->row_array();
    }
    function get_where($where){
      $this->db->where($where);
      $this->db->order_by($this->kd, $this->order);
      return $this->db->get($this->table)->result();
  }

      // get data by kd
  function get_by_user($kd,$dt){
      $this->db->where('karyawan.id_karyawan', $kd);
      $this->db->where('transaksi.tanggal', $dt);
      $this->db->join("dttransaksi", "transaksi.idTransaksi=dttransaksi.idTransaksi","left");
      $this->db->join("karyawan", "karyawan.id_karyawan=transaksi.user","left");
      $this->db->order_by('transaksi.jam', 'DESC');
      return $this->db->get($this->table);
  }
  function getAll($dt){
    $this->db->where('transaksi.tanggal', $dt);
    $this->db->order_by('karyawan.outlet', $this->order);
      $this->db->join("dttransaksi", "transaksi.idTransaksi=dttransaksi.idTransaksi","left");
      $this->db->join("karyawan", "karyawan.id_karyawan=transaksi.user","left");
      // $this->db->join("penjualan", "penjualan.idTransaksi=transaksi.idTransaksi");
      return $this->db->get($this->table);
  }

  function filter($ot){
    $dat = date('Y-m-d');
    $this->db->select("transaksi.tanggal, sum(dttransaksi.total) as 'total'");
    $this->db->from('dttransaksi');
    $this->db->join('transaksi', 'dttransaksi.idTransaksi=transaksi.idTransaksi', 'inner');
    $this->db->join('karyawan', 'karyawan.id_karyawan=transaksi.user', 'inner');
    $this->db->where('transaksi.tanggal', $dat);
    $this->db->where('karyawan.outlet', $ot);
  $query = $this->db->get();
  return $query->row_array();
  }

  }
