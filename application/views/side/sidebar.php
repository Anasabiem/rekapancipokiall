<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
      	<?php
            if ($this->session->userdata('status')== 'karyawan') {
              $dat = $this->M_user->selectwhere('karyawan',array('karyawan.id_karyawan' =>$this->session->userdata('id_karyawan')))->row();
              ?>
              <div class="pull-left image">
          <img style="height: 45px;" src="<?php echo base_url().$dat->foto ?>" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p><?php echo $dat->nama_karyawan; ?></p>
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
              <?php
            } else {
              $dat = $this->M_user->selectwhere('admin',array('admin.id_admin' =>$this->session->userdata('id_admin')))->row();
              ?>
              <div class="pull-left image">
          <img style="height: 45px;" src="<?php echo base_url() ?>master/dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p><?php echo $dat->nm_admin; ?></p>
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
              <?php
            }?>

      </div>
      <!-- search form -->
      <form action="#" method="get" class="sidebar-form">
        <div class="input-group">
          <input type="text" name="q" class="form-control" placeholder="Search...">
              <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
        </div>
      </form>
      <!-- /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu">
        <li class="header">MAIN NAVIGATION</li>
        <?php if ($this->session->userdata('status')=='karyawan'): ?>
        	<li class="<?php if ($this->uri->segment(1)=="" || $this->uri->segment(1)=="Home"): ?>
        	<?php echo 'active' ?>
        <?php endif ?> treeview">
          <a href="<?php echo base_url('Home') ?>">
            <i class="fa fa-dashboard"></i> <span>Kasir</span>
            <span class="pull-right-container">
              <!-- <i class="fa fa-angle-left pull-right"></i> -->
            </span>
          </a>
        </li>
        <li class="<?php if ($this->uri->segment(1)=="Laporan"): ?>
        	<?php echo 'active' ?>
        <?php endif ?>">
          <a href="<?php echo base_url('Laporan') ?>">
            <i class="fa fa-money"></i> <span>Laporan Kasir</span>
            <span class="pull-right-container">
              <!-- <small class="label pull-right bg-green">new</small> -->
            </span>
          </a>
        </li>
        <li class="<?php if ($this->uri->segment(1)=="Keluar"): ?>
          <?php echo 'active' ?>
        <?php endif ?>">
          <a href="<?php echo base_url('Keluar') ?>">
            <i class="fa fa-credit-card"></i> <span>Pengeluaran</span>
            <span class="pull-right-container">
              <!-- <small class="label pull-right bg-green">new</small> -->
            </span>
          </a>
        </li>
        <?php else: ?>


        	<li class="<?php if ($this->uri->segment(1)=="Dashboard"): ?>
        	<?php echo 'active' ?>
        <?php endif ?>">
          <a href="<?php echo base_url('Dashboard') ?>">
            <i class="fa fa-home"></i> <span>Dashboard</span>
            <span class="pull-right-container">
              <!-- <small class="label pull-right bg-green">new</small> -->
            </span>
          </a>
        </li>
        <li class="<?php if ($this->uri->segment(1)=="Laporan"): ?>
        	<?php echo 'active' ?>
        <?php endif ?>">
          <a href="<?php echo base_url('Laporan') ?>">
            <i class="fa fa-money"></i> <span>Laporan Penjualan</span>
            <span class="pull-right-container">
              <!-- <small class="label pull-right bg-green">new</small> -->
            </span>
          </a>
        </li>
        <li class="<?php if ($this->uri->segment(1)=="Keluar"): ?>
          <?php echo 'active' ?>
        <?php endif ?>">
          <a href="<?php echo base_url('Keluar') ?>">
            <i class="fa fa-credit-card"></i> <span>Pengeluaran</span>
            <span class="pull-right-container">
              <!-- <small class="label pull-right bg-green">new</small> -->
            </span>
          </a>
        </li>
         <li class="header">Lainnya</li>
         <li class="<?php if ($this->uri->segment(1)=="User"): ?>
          <?php echo 'active' ?>
        <?php endif ?>">
          <a href="<?php echo base_url('User') ?>">
            <i class="fa fa-th"></i> <span>Karyawan</span>
            <span class="pull-right-container">
              <!-- <small class="label pull-right bg-green">new</small> -->
            </span>
          </a>
        </li>
        <li class="<?php if ($this->uri->segment(2)=="menu"): ?>
          <?php echo 'active' ?>
        <?php endif ?>">
          <a href="<?php echo base_url('Dashboard/menu') ?>">
            <i class="fa fa-th"></i> <span>Menu</span>
            <span class="pull-right-container">
              <!-- <small class="label pull-right bg-green">new</small> -->
            </span>
          </a>
        </li>
        <?php endif ?>
        <!--  -->
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>
