<header class="main-header">
    <!-- Logo -->
    <a href="index2.html" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>CS</b></span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>CIPOKI</b>Site</span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>

      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <!-- Messages: style can be found in dropdown.less-->
          <!-- Notifications: style can be found in dropdown.less -->
                  <!-- <li class="dropdown notifications-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                      <i class="fa fa-bell-o"></i>
                      <span class="label label-warning">10</span>
                    </a>
                    <ul class="dropdown-menu">
                      <li class="header">You have 10 notifications</li>
                        <ul class="menu">
                          <li>
                            <a href="#">
                              <i class="fa fa-users text-aqua"></i> 5 new members joined today
                            </a>
                          </li>
                          <li>
                            <a href="#">
                              <i class="fa fa-warning text-yellow"></i> Very long description here that may not fit into the
                              page and may cause design problems
                            </a>
                          </li>
                          <li>
                            <a href="#">
                              <i class="fa fa-users text-red"></i> 5 new members joined
                            </a>
                          </li>
                          <li>
                            <a href="#">
                              <i class="fa fa-shopping-cart text-green"></i> 25 sales made
                            </a>
                          </li>
                          <li>
                            <a href="#">
                              <i class="fa fa-user text-red"></i> You changed your username
                            </a>
                          </li>
                        </ul>
                      </li>
                      <li class="footer"><a href="#">View all</a></li>
                    </ul>
                  </li> -->
          <!-- Tasks: style can be found in dropdown.less -->
          <!-- User Account: style can be found in dropdown.less -->
          <li class="dropdown user user-menu">
            <?php 
            if ($this->session->userdata('status')== 'karyawan') {
              $dat = $this->M_user->selectwhere('karyawan', array('karyawan.id_karyawan' =>$this->session->userdata('id_karyawan') ))->row();?>

                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                <img src="<?php echo base_url().$dat->foto ?>" class="user-image" alt="User Image">
              <span class="hidden-xs"><?php echo $dat->nama_karyawan; ?></span></a>

            <?php } else {
              $dat = $this->M_user->selectwhere('admin',array('admin.id_admin' =>$this->session->userdata('id_admin') ))->row();?>
              <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                <img src="<?php echo base_url() ?>master/dist/img/user2-160x160.jpg" class="user-image" alt="User Image">
              <span class="hidden-xs"><?php echo $dat->nm_admin; ?></span>
              
            </a>
           <?php }
            
                                                 ?>
            
            <ul class="dropdown-menu">
              <!-- User image -->
              <?php if ($this->session->userdata('status')== 'karyawan'): ?>
                <li class="user-header">
                <img src="<?php echo base_url().$dat->foto ?>" class="img-circle" alt="User Image">

                <p>
                  <?php echo $dat->nama_karyawan; ?>
                  <small><?php echo $this->session->userdata('status') ?></small>
                </p>
              </li>
              <?php else: ?>
                <li class="user-header">
                <img src="<?php echo base_url() ?>master/dist/img/user2-160x160.jpg" class="img-circle" alt="User Image"> 

                <p>
                  <?php echo $dat->nm_admin; ?>
                  <small>Admin</small>
                </p>
              </li>
              <?php endif ?>
              
              <!-- Menu Footer-->
              <li class="user-footer">
                <div class="pull-left">
                  <a href="<?php echo base_url('User/profile') ?>" class="btn btn-default btn-flat">Profile</a>
                </div>
                <div class="pull-right">
                  <a href="<?php echo base_url('Login/logout') ?>" class="btn btn-default btn-flat">Sign out</a>
                </div>
              </li>
            </ul>
          </li>
          <!-- Control Sidebar Toggle Button -->
          <li>
            <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
          </li>
        </ul>
      </div>
    </nav>
  </header>