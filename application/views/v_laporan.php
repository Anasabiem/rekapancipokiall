<?php $this->load->view('side/headd'); ?>
<?php $this->load->view('side/header'); ?>
<?php $this->load->view('side/sidebar'); ?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
		<section class="content">

      <div class="row">
        <div class="col-xs-12">
          <div class="box"><div class="panel-heading">Laporan Khusus Untuk Penjualan Hari Ini</div>
            <div class="box-header">
            	<div class="col-md-12">
                <?php if ($this->session->userdata('status')== 'karyawan'): ?>
                  <p class="box-title">Pemasukan Uang</p>
                   <i style="font-size: 20px; font-style: italic; font-weight: bold;"> Rp. <?php echo number_format($total_uang['total']); ?></i>
                  <p><?php echo $total_asset; ?> Transaksi</p>
                <?php else: ?>
                  <div class="col-md-4">
                    <p class="box-title">Pemasukan Uang Total</p><br>
                    <i style="font-size: 20px; font-style: italic; font-weight: bold;"> Rp. <?php echo number_format($total_uangadm['total']); ?></i>
                  <p><?php echo $total_assetadm; ?> Transaksi</p>
                  </div>

                  

                <?php endif ?>

            	</div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example2" class="table table-bordered table-hover">
                <thead>
                <tr>
                  <?php if ($this->session->userdata('status') == 'karyawan'): ?>
                  <th>Nama Pembeli</th>
                  <th>Waktu</th>
                  <th>Jumlah Pesanan (Rp)</th>
                  <?php else: ?>
                    <th>Outlet</th>
                  <th>Nama Pembeli</th>
                  <th>Waktu</th>
                  <th>Jumlah Pesanan (Rp)</th>
                  <?php endif ?>

                  <!-- <th>Jenis Pesanan</th> -->
                </tr>
                </thead>
                <?php $dataaa=$this->session->userdata('status');

                if ($dataaa == 'karyawan') { ?>
                  <tbody>
                    <!-- <td><?php echo $key->outlet; ?></td> -->
                  <?php foreach ($isi->result() as $key): ?>
                    <tr>
                  <td><?php echo $key->nmPembeli; ?></td>
                  <td><?php echo $key->jam; ?></td>
                  <td>Rp. <?php echo number_format($key->total); ?></td>
                  <!-- <td><?php echo $key->jenis; ?></td> -->
                  <!-- <td>X</td> -->
                </tr>
                  <?php endforeach ?>
                </tbody>
                 <?php } else { ?>
                   <tbody>
                  <?php foreach ($isiad->result() as $key): ?>
                    <tr>
                      <td><?php echo $key->outlet; ?></td>
                  <td><?php echo $key->nmPembeli; ?></td>
                  <td><?php echo $key->jam; ?></td>
                  <td> <a href="" type="button" data-toggle="modal" data-target="#exampleModalCenter1<?php echo $key->idTransaksi; ?>">Rp. <?php echo number_format($key->total); ?></a>
                <div class="modal fade" id="exampleModalCenter1<?php echo $key->idTransaksi; ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                  <div class="modal-dialog modal-dialog-centered" role="document">
                    <div class="modal-content">
                      <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalCenterTitle">Detail Transaksi</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>
                      </div>
                      <div class="modal-body">
                       <table id="example1" class="table table-bordered table-striped">
                        <thead>
                          <tr>
                            <th>Nama Barang</th>
                            <th>Saus</th>
                            <th>Level</th>
                            <th>Harga</th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <th><?php echo $key->nama_menu ?></th>
                            <th><?php echo $key->saos ?></th>
                            <th><?php echo $key->level ?></th>
                            <th><?php echo number_format($key->harga) ?></th>
                          </tr>
                        </tbody>
                      </table>
                      </div>

                    </div>
                  </div>
                </div>
                  </td>
                  <!-- <td><?php echo $key->jenis; ?></td> -->
                  <!-- <td>X</td> -->
                </tr>
                  <?php endforeach ?>
                </tbody>
                 <?php }
                  ?>

                  <!-- <th>Jenis Pesanan</th> -->
                </tr>
                </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
</div>
      <!-- /.row -->
    </section>
</div>
<!-- /.content-wrapper -->
<footer class="main-footer">
	<div class="pull-right hidden-xs">

	</div>
	<strong>Copyright &copy; 2014-2019 <a href="">Cipoki Site</a>.</strong> All rights
	reserved.
</footer>
<script>
		$(function () {
			$("#example1").DataTable();
			$('#example2').DataTable({
				"paging": true,
				"lengthChange": false,
				"searching": false,
				"ordering": true,
				"info": true,
				"autoWidth": false
			});
		});
	</script>
<?php $this->load->view('side/js'); ?>
