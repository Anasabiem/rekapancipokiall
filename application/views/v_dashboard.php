<?php $this->load->view('side/headd'); ?>
<?php $this->load->view('side/header'); ?>
<?php $this->load->view('side/sidebar'); ?>

<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <section class="content-header">
      <h1>
        Dashboard
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
      <div class="row">
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-aqua">
            <div class="inner">
              <h4>Rp. <?php echo number_format($total_uangadm['total']); ?><sup style="font-size: 20px"> -,</sup></h4>
              <p><?php echo $total_assetadm; ?> Transaksi</p>
              <p>Pemasukan </p>
            </div>
            <div class="icon">
              <i class="ion ion-bag"></i>
            </div>
            <a href="<?php echo base_url('Laporan'); ?>" class="small-box-footer">Detail <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-green">
            <div class="inner">
              <h4>Rp. <?php echo number_format($total_uangadm1['total']) ?><sup style="font-size: 20px"> -,</sup></h4>
              <p><?php echo $total_assetadmkl; ?> pengeluaran</p>
              <p>Pengeluaran</p>
            </div>
            <div class="icon">
              <i class="ion ion-stats-bars"></i>
            </div>
            <a href="<?php echo base_url('Keluar'); ?>" class="small-box-footer">Detail<i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-yellow">
            <div class="inner">
              <?php $i= $total_uangadm['total'] - $total_uangadm1['total']; ?>
              <h4>Rp. <?php echo number_format($i); ?> <sup style="font-size: 20px"> -,</sup></h4>
              <p>Hitungan Dalam Hari</p>
              <p>Laba</p>
            </div>
            <div class="icon">
              <i class="ion ion-person-add"></i>
            </div>
            <a href="" class="small-box-footer"><i class="fa fa-arrow-circle-right"></i><i class="fa fa-arrow-circle-left"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <!-- ./col -->
      </div>
      <!-- /.row -->


      <!-- Main row -->
      <div class="row">
        <section class="col-lg-12 connectedSortable">
        <div class="box box-solid bg-light-blue-gradient">
            <div class="box-header">
              <!-- tools box -->
              <div class="pull-right box-tools">
                <button type="button" class="btn btn-primary btn-sm daterange pull-right" data-toggle="tooltip" title="Date range">
                  <i class="fa fa-calendar"></i></button>
                <button type="button" class="btn btn-primary btn-sm pull-right" data-widget="collapse" data-toggle="tooltip" title="Collapse" style="margin-right: 5px;">
                  <i class="fa fa-minus"></i></button>
              </div>
              <!-- /. tools -->

              <i class="fa fa-map-marker"></i>

              <h3 class="box-title">
                Laporan Keuangan Outlet
              </h3>
            </div>
            <div class="box-body">
                    <table id="example2" class="table table-bordered table-hover">
                      <thead>
                        <tr>
                          <td>Outlet</td>
                          <td>Pemasukan</td>
                          <td>Pengeluaran</td>
                          <td>Laba / Rugi</td>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td>Jawa</td>
                          <td>Rp. <?php echo number_format($jawam['total']) ?></td>
                          <td>Rp. <?php if ($jawak['total'] == 0): ?>
                            <?php echo "0" ?>
                          <?php else: ?>
                            <?php echo number_format($jawak['total']) ?>
                          <?php endif; ?> </td>
                          <td>Rp. <?php $hasil = $jawam['total']-$jawak['total']; echo number_format($hasil) ?></td>
                        </tr>
                        <tr>
                          <td>Polres</td>
                          <td>Rp. <?php echo number_format($polresm['total']) ?></td>
                          <td>Rp. <?php if ($polresk['total'] == 0): ?>
                            <?php echo "0" ?>
                          <?php else: ?>
                            <?php echo number_format($polresk['total']) ?>
                          <?php endif; ?> </td>
                          <td>Rp. <?php $hasil = $polresm['total']-$polresk['total']; echo number_format($hasil) ?></td>
                        </tr>
                        <tr>
                          <td>Patrang</td>
                          <td>Rp. <?php echo number_format($patrangm['total']) ?></td>
                          <td>Rp. <?php if ($patrangk['total'] == 0): ?>
                            <?php echo "0" ?>
                          <?php else: ?>
                            <?php echo number_format($patrangk['total']) ?>
                          <?php endif; ?> </td>
                          <td>Rp. <?php $hasil = $patrangm['total']-$patrangk['total']; echo number_format($hasil) ?></td>
                        </tr>
                      </tbody>
                    </table>
            </div>
            <!-- /.box-body-->
            <div class="box-footer no-border">
              <!-- /.row -->
            </div>
          </div>
        </section>
        <!-- Left col -->
        <section class="col-lg-12 connectedSortable">
          <div class="row">
            <div class="col-md-12" >
              <!-- AREA CHART -->
              <div class="box box-primary">
                <div class="box-header with-border">
                  <h3 class="box-title">Area Chart</h3>

                  <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                    </button>
                    <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                  </div>
                </div>
                <div class="box-body chart-responsive">
                  <div class="chart" id="revenue-chart" style="height: 300px;"></div>
                </div>
                <!-- /.box-body -->
              </div>
              <!-- /.box -->
            </div>
          </div>
          <!-- /.row -->
        </section>
        <!-- right col -->
      </div>
      <!-- /.row (main row) -->

    </section>
  </div>
  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <div class="pull-right hidden-xs">

    </div>
    <strong>Copyright &copy; 2014-2019 <a href="">Cipoki Site</a>.</strong> All rights
    reserved.
  </footer>
<?php $this->load->view('side/js'); ?>
<!-- Morris.js charts -->
<!-- page script -->
  <?php if ($this->session->flashdata()) { ?>
                        <?php echo $this->session->flashdata('Pesan'); ?>
                    <?php } ?>
