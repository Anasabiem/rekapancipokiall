<?php $this->load->view('side/headd'); ?>
<?php $this->load->view('side/header'); ?>
<?php $this->load->view('side/sidebar'); ?>
  <div class="content-wrapper">
 <section class="content">
      <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Create Karyawan</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form role="form" method="post" action="<?php echo base_url('User/buat') ?>" enctype="multipart/form-data">
              <div class="box-body">
                <div class="form-group">
                  <label for="exampleInputEmail1">Nama Karyawan</label>
                  <input type="text" class="form-control" name="nama" required="">
                </div>
                <div class="form-group">
                  <label for="exampleInputEmail1">Alamat</label>
                  <input type="text" class="form-control" name="alamat" required="">
                </div>
                <div class="form-group">
                  <label for="exampleInputEmail1">Username</label>
                  <input type="text" class="form-control" name="username" required="">
                </div>
                <div class="form-group">
                  <label for="exampleInputPassword1">Password</label>
                  <input type="password" class="form-control" name="password" required="">
                </div>
                <div class="form-group">
                  <label for="exampleInputEmail1">Nama Partner</label>
                  <input type="text" class="form-control" name="partner" required="">
                </div>
                <div class="form-group">
                  <label for="exampleInputEmail1">Bagian</label>
                  <select class="form-control" name="bagian" required="">
                  <option value="Outlet Keeper">Outlet Keeper</option>
                  <option value="Delivery Man">Delivery Man</option>
                  <!-- <option value="Polres">Polres</option> -->
                  </select>
                </div>
                <div class="form-group">
                  <label for="exampleInputEmail1">Outlet</label>
                  <select class="form-control" name="out" required="">
                  <option value="jawa">Jawa</option>
                  <option value="patrang">Patrang</option>
                  <option value="Polres">Polres</option>
                  </select>
                </div>
                <div class="form-group">
                  <label >File input</label>
                  <input name="foto" type="file" id="exampleInputFile" required="">

                  <p class="help-block">Example block-level help text here.</p>
                </div>
                <!-- <div class="checkbox">
                  <label>
                    <input type="checkbox"> Check me out
                  </label>
                </div> -->
              </div>

              <div class="box-footer" >
                <button type="submit" style="float: right;" name="btnSimpan" class="btn btn-primary">Submit</button>
              </div>
            </form>
          </div>
        </div>
      </div>
      <!-- /.row -->
    </section>   
  </div>
  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      
    </div>
    <strong>Copyright &copy; 2014-2019 <a href="">Cipoki Site</a>.</strong> All rights
    reserved.
  </footer>
<?php $this->load->view('side/js'); ?>
