<?php $this->load->view('side/headd'); ?>
<?php $this->load->view('side/header'); ?>
<?php $this->load->view('side/sidebar'); ?>
<div class="content-wrapper">
		<section class="content">
<section class="content-header">
      <h1>
        User Profile
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Examples</a></li>
        <li class="active">User profile</li>
      </ol>
    </section>
    <section class="content">

      <div class="row">
        <div class="col-md-3">

          <!-- Profile Image -->
          <?php 
            if ($this->session->userdata('status')== 'karyawan') {
              $dat = $this->M_user->selectwhere('karyawan',array('karyawan.id_karyawan' =>$this->session->userdata('id_karyawan')))->row();
              ?>
              <div class="box box-primary">
            <div class="box-body box-profile">
              <img style="height: 100px;" class="profile-user-img img-responsive img-circle" src="<?php echo base_url().$dat->foto ?>" alt="User profile picture">

              <h3 class="profile-username text-center"><?php echo $dat->nama_karyawan; ?></h3>

              <p class="text-muted text-center"><?php echo $this->session->userdata('status') ?></p>

              <a href="#" class="btn btn-primary btn-block" data-toggle="modal" data-target="#exampleModal"><b>Ganti Foto</b></a>

              <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Ganti Foto Profile</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                  <form method="post" action="<?php echo base_url('User/updateft') ?>" enctype="multipart/form-data">
                  <div class="modal-body">
                    <h5>Pilih Foto</h5>
                    <input type="file" name="ftProfil">
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="Submit" class="btn btn-primary" name="">Save changes</button>
                  </div>
                  </form>
                </div>
              </div>
            </div>
            </div>
            <!-- /.box-body -->
          </div>
            <?php } else {
              $dat = $this->M_user->selectwhere('admin',array('admin.id_admin' =>$this->session->userdata('id_admin')))->row();?>
              <div class="box box-primary">
            <div class="box-body box-profile">
              <img style="height: 100px;" class="profile-user-img img-responsive img-circle" src="<?php echo base_url() ?>master/dist/img/user2-160x160.jpg" alt="User profile picture">

              <h3 class="profile-username text-center"><?php echo $dat->nm_admin; ?></h3>

              <a href="#" class="btn btn-primary btn-block"><b>Follow</b></a>
            </div>
            <!-- /.box-body -->
          </div>
            <?php }?>
          <!-- /.box -->

          <!-- About Me Box -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">About Me</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <strong><i class="fa fa-book margin-r-5"></i> Education</strong>

              <p class="text-muted">
                B.S. in Computer Science from the University of Tennessee at Knoxville
              </p>

              <hr>

              <strong><i class="fa fa-map-marker margin-r-5"></i> Location</strong>

              <p class="text-muted">Malibu, California</p>

              <hr>

              <strong><i class="fa fa-pencil margin-r-5"></i> Skills</strong>

              <p>
                <span class="label label-danger">UI Design</span>
                <span class="label label-success">Coding</span>
                <span class="label label-info">Javascript</span>
                <span class="label label-warning">PHP</span>
                <span class="label label-primary">Node.js</span>
              </p>

              <hr>

              <strong><i class="fa fa-file-text-o margin-r-5"></i> Notes</strong>

              <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam fermentum enim neque.</p>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
        <div class="col-md-9">
          <div class="nav-tabs-custom">
          	<?php if ($this->session->userdata('status')=='karyawan'): ?>
            		<ul class="nav nav-tabs">
              <li class="active"><a href="#activity" data-toggle="tab">Personal</a></li>
              <li><a href="#settings" data-toggle="tab">Akun</a></li>
            </ul>
            <div class="tab-content">
              <div class="active tab-pane" id="activity">
                <!-- Post -->
                <div class="form-group has-success">
                  <label class="control-label" for="inputSuccess"><i class="fa fa-user"></i> Nama</label>
                  <input type="text" class="form-control" id="inputSuccess" placeholder="Enter ..." value="<?php echo $dat->nama_karyawan ?>">
                </div>
                <div class="form-group has-success">
                  <label class="control-label" for="inputSuccess"><i class="fa fa-check"></i> Alamat</label>
                  <input type="text" class="form-control" id="inputSuccess" placeholder="Enter ..." value="<?php echo $dat->alamat ?>">
                </div>
                <div class="form-group has-success">
                  <label class="control-label" for="inputSuccess"><i class="fa fa-check"></i> Bagian Dalam Pekerjaan</label>
                  <input type="text" class="form-control" id="inputSuccess" placeholder="Enter ..." value="<?php echo $dat->bagian ?>">
                </div>
                <div class="form-group has-success">
                  <label class="control-label" for="inputSuccess"><i class="fa fa-check"></i> Penempatan Outlet</label>
                  <input type="text" class="form-control" id="inputSuccess" placeholder="Enter ..." value="<?php echo $dat->outlet ?>">
                </div>
                <!-- /.post -->
              </div>
              <!-- /.tab-pane -->

              <div class="tab-pane" id="settings">
                <form class="form-horizontal">
                  <div class="form-group">
                    <label for="inputName" class="col-sm-2 control-label">Username</label>

                    <div class="col-sm-10">
                      <input type="text" class="form-control" id="inputName" value="<?php echo $dat->username ?>">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputEmail" class="col-sm-2 control-label">Nomor Hp</label>

                    <div class="col-sm-10">
                      <input type="number" class="form-control" id="inputEmail" value="<?php echo $dat->noHp ?>">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputEmail" class="col-sm-2 control-label">Password</label>

                    <div class="col-sm-10">
                      <input type="text" class="form-control" id="inputEmail" value="*********************">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputExperience" class="col-sm-2 control-label">Hint</label>

                    <div class="col-sm-10">
                      <textarea class="form-control" id="inputExperience" placeholder="Experience"><?php echo $dat->hint ?></textarea>
                    </div>
                  </div>
                  <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                      <button type="submit" class="btn btn-primary" style="float: right;"><i class="fa fa-pencil"></i> Ubah Data</button>
                    </div>
                  </div>
                </form>
              </div>
              <!-- /.tab-pane -->
            </div>
            	<?php else: ?>
            		<?php $dat = $this->M_user->selectwhere('admin',array('admin.id_admin' =>$this->session->userdata('id_admin')))->row();?> 
            		<ul class="nav nav-tabs">
              <!-- <li class="active"><a href="#activity" data-toggle="tab">Personal</a></li> -->
              <li class="active"><a href="#settings" data-toggle="tab">Akun</a></li>
            </ul>
              <!-- /.tab-pane -->

              <div class="tab-pane" id="settings">
                <form class="form-horizontal">
                	<div class="form-group">
                    <label for="inputName" class="col-sm-2 control-label">Nama Admin</label>

                    <div class="col-sm-10">
                      <input type="text" class="form-control" id="inputName" value="<?php echo $dat->nm_admin ?>">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputName" class="col-sm-2 control-label">Username</label>

                    <div class="col-sm-10">
                      <input type="text" class="form-control" id="inputName" value="<?php echo $dat->username ?>">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputEmail" class="col-sm-2 control-label">Password</label>

                    <div class="col-sm-10">
                      <input type="text" class="form-control" id="inputEmail" value="*********************">
                    </div>
                  </div>
                  
                  <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                      <button type="submit" class="btn btn-primary" style="float: right;"><i class="fa fa-pencil"></i> Ubah Data</button>
                    </div>
                  </div>
                </form>
              </div>
              <!-- /.tab-pane -->
            </div>
            	<?php endif ?>
            <!-- /.tab-content -->
          </div>
          <!-- /.nav-tabs-custom -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

    </section>
		</section>
	</div>



<?php $this->load->view('side/js'); ?>