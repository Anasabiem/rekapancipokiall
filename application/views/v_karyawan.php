<?php $this->load->view('side/headd'); ?>
<?php $this->load->view('side/header'); ?>
<?php $this->load->view('side/sidebar'); ?>
<div class="content-wrapper">

	<div class="box">
		<div class="box-header">
			<h3 class="box-title">Data Karyawan</h3>
		</div>
		<div class="col-md-3"></div>
		<div class="box-body col-md-6">
			<a class="btn btn-block btn-social btn-bitbucket" href="<?php echo base_url('User/insert') ?>">
				<i class="fa fa-plus"></i> Tambah Karyawan
			</a>
		</div>
		<!-- /.box-header -->
		<div class="box-body">
			<table id="example1" class="table table-bordered table-striped">
				<thead>
					<tr>
						<th>Nama</th>
						<th>Outlet</th>
						<th>Status</th>
						<!-- <th>Bagian</th> -->
						<th>Action</th>
					</tr>
				</thead>
				<tbody>
					<?php foreach ($karyawan->result() as $key): ?>
						<tr>
							<td><?php echo $key->nama_karyawan; ?></td>
							<td><?php echo $key->outlet; ?></td>
							<td><?php if ( $key->status == 'login'): ?>
							<i class="fa fa-circle text-success"></i> Online
							<?php else: ?>
								<i class="fa fa-circle text-danger"></i> Offline
								<?php endif ?></td>
								<td><a href="#" onclick="deleted('<?php echo $key->id_karyawan; ?>')"><i class="fa fa-close"></i></a></a>
									<a class="btn btn-social-icon btn-bitbucket" title="Edit"><i class="fa fa-pencil"></i></a>
									<!-- <a class="btn btn-social-icon btn-bitbucket"><i class="fa fa-bitbucket"></i></a> -->
								</td>
							</tr>
						<?php endforeach ?>
					</tbody>
				</table>
			</div>
			<!-- /.box-body -->
		</div>
	</div>
	<!-- /.content-wrapper -->
	<footer class="main-footer">
		<div class="pull-right hidden-xs">

		</div>
		<strong>Copyright &copy; 2014-2019 <a href="">Cipoki Site</a>.</strong> All rights
		reserved.
	</footer>
	<script type="text/javascript">
	  function deleted(param){
	    var proc = window.confirm('Apakah Anda Yakin Menghapus Data Ini?');
	    if (proc) {
	      document.location='<?php echo base_url(); ?>User/hps/'+param;
	    }
	  }
	  function update(param){
	    document.location='<?php echo base_url(); ?>Admin/Mitra/verifikasi/'+param;
	  }
	</script>
	<script>
		$(function () {
			$("#example1").DataTable();
			$('#example2').DataTable({
				"paging": true,
				"lengthChange": false,
				"searching": false,
				"ordering": true,
				"info": true,
				"autoWidth": false
			});
		});
	</script>
	<?php $this->load->view('side/js'); ?>
