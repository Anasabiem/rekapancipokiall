<?php $this->load->view('side/headd'); ?>
<?php $this->load->view('side/header'); ?>
<?php $this->load->view('side/sidebar'); ?>

<!-- Content Wrapper. Contains page content -->
<?php if ($this->session->userdata('hint')==null): ?>
	<form method="POST" action="<?php echo base_url('Home/hint') ?>">
		<div id="modal" class="modal fade" tabindex="-1" role="dialog">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title">Password Hint</h4>
						<i>Password Hint ini berfungsi Apabila anda mengalami lupa password maka anda harus menggunakan jawaban dari pertanyaan ini.</i>
					</div>
					<div class="modal-body">
						<p>Siapakah nama Ibu Anda ?  </p>
						<input type="text" name="hint" class="form-control" required="">

					</div>
					<div class="modal-footer">
						<!-- <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> -->
						<button type="Submit" class="btn btn-primary">Save changes</button>
					</div>
				</div><!-- /.modal-content -->
			</div><!-- /.modal-dialog -->
		</div><!-- /.modal -->
	</form>
<?php endif ?>

<?php if ($this->session->userdata('status')=='karyawan'): ?>
<div class="content-wrapper">

	<div class="panel panel-default">
		<div class="panel-heading">Pilih Menu, Pilih Saus & Tentukan Level Pedasnya
			<!-- <i style="float: right;"><input type="checkbox"> Go Jek / Grabb</i> -->
		</div>
		<form action="<?php echo base_url('Home/masuk') ?>" method="POST">
			<div class="panel-body">
				<div class="box-body">
					<div class="col-md-6">
						<div class="input-group">
							<!-- <div class="col-md-6"> -->
								<div class="input-group-btn">
									<button type="button" class="btn btn-danger">Nama Pemesan</button>
								</div>
								<input type="text" name="nmPs" class="form-control" placeholder="Nama Pemesan" required="">
							</div>
						</div>

						<p >Total Pembelian : <i style="font-size: 30px;">Rp. <i class="total" ></i> </i></p>
						<input type="hidden" name="totalhar" class="total" value="">
						<!-- </div> -->

					</div>
					<br><br>
					<div class="after-add-more">
						<div class="input-group control-group ">

							<div class="col-md-12">
								<div class="col-md-3">
									<label>Menu</label>
									<select urutan="0" class="form-control pilih makan makanan" required="" name="menu[]" >
										<option style="font-weight: bold;" readonly>Pilih Menu</option>
										<?php foreach ($menu as $key): ?>
											<option harga="<?php echo $key->harga; ?>"  value="<?php echo $key->namaMenu; ?>"><?php echo $key->namaMenu; ?></option>
										<?php endforeach ?>
									</select>
									<!-- <input type="text" name="addmore[]" class="form-control" placeholder="Hobi"> -->
								</div>
								<div class="col-md-3">
									<label>Saos</label>
									<select class="form-control saos" required="" name="saos[]">
										<option value="" style="font-weight: bold;">Pilih Saos</option>
										<option value="Spageti">Spageti</option>
										<option value="Sea Food">Sea food</option>
										<option value="Cheeze">Cheeze</option>
									</select>
									<!-- <input type="text" name="addmore[]" class="form-control" placeholder="Hobi"> -->
								</div>
								<div class="col-md-3">
									<label>Level</label>
									<select class="form-control level" required="" name="sambal[]" >
										<option value="" style="font-weight: bold;">Pilih Level</option>
										<option value="0">0</option>
										<option value="1">1</option>
										<option value="1">2</option>
										<option value="1">3</option>
										<option value="1">4</option>
										<option value="1">5</option>

									</select>
									<!-- <input type="text" name="addmore[]" class="form-control" placeholder="Hobi"> -->
								</div>
								<div class="col-md-3">
									<label>Harga</label><br>
									<label>Rp.<label class="hargaq0"></label></label>
									<input type="hidden" name="harga[]" class="form-control harga0 angka" value="0">
								</div>
							</div>
							<div class="input-group-btn">
								<button class="btn btn-success add-more" type="button" ><i class="fa fa-plus"></i> Tambah</button>
							</div>
						</div>
					</div>

					<div class="control-group text-center">
						<br>
						<button class="btn btn-success" type="button" data-toggle="modal" data-target="#exampleModal">Submit</button>
						<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
							<div class="modal-dialog" role="document">
								<div class="modal-content">
									<div class="modal-header">
										<h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
										<button type="button" class="close" data-dismiss="modal" aria-label="Close">
											<span aria-hidden="true">&times;</span>
										</button>
									</div>
									<div class="modal-body">
										<h5>Total Pembelian <br>Rp. <i style="font-size: 30px;" class="total"></i></h5>
										<label>Bayar</label><input type="number" required="" name="bayar" class="form-control">
									</div>
									<div class="modal-footer">
										<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
										<button type="Submit" class="btn btn-primary">Save changes</button>
									</div>
								</div>
							</div>
						</div>
					</div>


					<!-- Copy Fields -->

				</div>
			</form>
			<!-- </div> -->
		</div>
	</div>
<?php endif ?>

	<!-- /.content-wrapper -->
	<footer class="main-footer">
		<div class="pull-right hidden-xs">

		</div>
		<strong>Copyright &copy; 2014-2019 <a href="">Cipoki Site</a>.</strong> All rights
		reserved.
	</footer>
	<script type="text/javascript">
		$(document).ready(function() {
			$('#modal').modal('show');
			$(".add-more").click(function(){
          // var html = $(".copy").html();
          var menu = $('.makanan').html();
          var saos = $('.saos').html();
          var level = $('.level').html();
          // alert(level);
          var i = 0;

          $('.makan').each(function(){
          	i++;
          });
          // alert(i);
          var html = '<div class="control-group input-group" style="margin-top:10px">'+
          '<div class="col-md-12">'+
          '<div class="col-md-3">'+
          '<label>Menu</label>'+
          '<select urutan="'+i+'" class="form-control makan pilih" name="menu[]">'
          +menu+
          '</select>'+
          '</div>'+
          '<div class="col-md-3">'+
          '<label>Saos</label>'+
          '<select class="form-control" name="saos[]">'
          +saos+
          '</select>'+
          '</div>'+
          '<div class="col-md-3">'+
          '<label>Level</label>'+
          '<select class="form-control" name="sambal[]">'
          +level+
          '</select>'+
          '</div>'+
          '<div class="col-md-3">'+
          '<label>Harga</label><br>'+
          '<label>Rp.<label class="hargaq'+i+'"></label></label>'+
          '<input type="hidden" name="harga[]" class="form-control angka harga'+i+'"  value="0" >'+
          '</div>'+
          '</div>'+
          '<div class="input-group-btn">'+
          '<button class="btn btn-danger remove" type="button"><i class="glyphicon glyphicon-remove"></i> Hapus</button>'+
          '</div>'+
          '</div>';
          // alert(html);
          $(".after-add-more").append(html);
      });
			function apa(){
				var totalharga = 0;
				$('.angka').each(function(){
					totalharga +=parseInt($(this).val());
      			// alert(totalharga);
      			$(".total").text(totalharga);
      			$(".total").val(totalharga);

      		});
			}
			$("body").on("click",".remove",function(){
				$(this).parents(".control-group").remove();
				apa();
			});
			$(document).on('change','.pilih',function(){
				var id = $('option:selected',this).attr("harga");
    	// alert(id);
    	var urutan = $(this).attr('urutan');
    	// $(".hargaq").text(id);
    	// $(".harga").val(id);

    	$(".harga"+urutan).val(id);
    	$(".hargaq"+urutan).text(id);
    	apa();

    });
		});
	</script>
	<?php $this->load->view('side/js'); ?>
	<?php if ($this->session->flashdata()) { ?>
		<?php echo $this->session->flashdata('Pesan'); ?>
		<?php } ?>
