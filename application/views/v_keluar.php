<?php $this->load->view('side/headd'); ?>
<?php $this->load->view('side/header'); ?>
<?php $this->load->view('side/sidebar'); ?>
<div class="content-wrapper">
  <div class="box">
    <div class="box-header">
      <h3 class="box-title">Data Pengeluaran Outlet <i style="font-size: 12px">Harian</i></h3>
    </div>
    <div class="box-header">
      <div class="col-md-4">
        <p class="box-title">Pengeluaran Uang</p>
        <?php if ($this->session->userdata('status')=='karyawan'): ?>
          <i style="font-size: 20px; font-style: italic; font-weight: bold;"> Rp. <?php echo number_format($total_uang['total']); ?> </i>
          <p> <?php echo $total_asset; ?> Barang yang dibeli</p>
          <?php else: ?>
            <i style="font-size: 20px; font-style: italic; font-weight: bold;"> Rp. <?php echo number_format($total_uangadm['total']); ?> </i>
            <p> <?php echo $total_assetadm; ?> Barang yang dibeli</p>
          <?php endif ?>


        </div>
      </div>
      <br>
      <?php if ($this->session->userdata('status')=='karyawan'): ?>
        <div style="margin-left: 30px;">
          <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModalCenter">
            Tambah Pengeluaran
          </button>
          <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <h5 class="modal-title" id="exampleModalCenterTitle">Modal title</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body">
                 <form role="form" method="post" action="<?php echo base_url('Keluar/insert') ?>" enctype="multipart/form-data">
                  <div class="box-body">
                    <div class="form-group">
                      <label for="exampleInputEmail1">Nama Barang</label>
                      <input type="text" class="form-control" name="nama" required="">
                    </div>
                    <div class="form-group">
                      <label for="exampleInputEmail1">Kuantitas</label>
                      <input type="number" class="form-control" name="ktt" required="">
                    </div>
                    <div class="form-group">
                      <label for="exampleInputEmail1">Harga Satuan <i>(Kg/gr/biji)</i></label>
                      <input type="number" class="form-control" name="hrga" required="">
                    </div>
                  </div>

                  <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save changes</button>
                  </div>
                </form>
              </div>

            </div>
          </div>
        </div>
      </div>
    <?php endif ?>

    <!-- /.box-header -->
    <div class="box-body">
      <table id="example1" class="table table-bordered table-striped">
        <?php if ($this->session->userdata('status')=='karyawan'): ?>
          <thead>
            <tr>
              <th>Nama Barang</th>
              <th>Jam</th>
              <th>Kuantitas</th>
              <!-- <th>Harga Satuan <i>(Kg/gr/biji)</i></th> -->
              <th>Total Uang</th>
            </tr>
          </thead>
          <?php foreach ($isi->result() as $key): ?>
            <tbody>
              <tr>
                <td><?php echo $key->nmBarang; ?></td>
                <td><?php echo $key->jam; ?>
              </td>
              <td><?php echo $key->qty; ?></td>
              <td>Rp. <?php echo number_format($key->ttlUang); ?></td>
              <!-- <td>X</td> -->
            </tr>

          </tbody>
        <?php endforeach ?>

        <?php else: ?>
          <thead>
            <tr>
              <td>Outlet</td>
              <th>Nama Barang</th>
              <th>Jam</th>
              <th>Kuantitas</th>
              <th>Total Uang</th>
            </tr>
          </thead>
          <?php foreach ($isiad->result() as $key): ?>
            <tbody>
              <tr>
                <td><?php echo $key->outlet; ?></td>
                <td><?php echo $key->nmBarang; ?></td>
                <td><?php echo $key->jam; ?>
              </td>
              <td><?php echo $key->qty; ?></td>
              <td>Rp, <?php echo number_format($key->ttlUang); ?> 
              </td>

            </tr>

          </tbody>
        <?php endforeach ?>

      <?php endif ?>

    </table>
  </div>
  <!-- /.box-body -->
</div>
</div>
<footer class="main-footer">
  <div class="pull-right hidden-xs">

  </div>
  <strong>Copyright &copy; 2014-2019 <a href="">Cipoki Site</a>.</strong> All rights
  reserved.
</footer>
<script>
  $(function () {
    $("#example1").DataTable();
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false
    });
  });
</script>
<?php $this->load->view('side/js'); ?>
<?php if ($this->session->flashdata()) { ?>
  <?php echo $this->session->flashdata('Pesan'); ?>
<?php } ?>
